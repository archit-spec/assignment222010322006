def roman_to_integer(A):
    roman_values = {'I': 1, 'V': 5, 'X': 10, 'L': 50,
                    'C': 100, 'D': 500, 'M': 1000}

    result = 0
    prev_value = 0

    # Iterate through the string from right to left
    for char in reversed(A):
        current_value = roman_values[char]

        # If the current value is less than the previous value, subtract it
        if current_value < prev_value:
            result -= current_value
        else:
            result += current_value

        prev_value = current_value

    return result

# Example usage:
input_str_1 = "XIV"
output_1 = roman_to_integer(input_str_1)
print(output_1)

input_str_2 = "XX"
output_2 = roman_to_integer(input_str_2)
print(output_2)

