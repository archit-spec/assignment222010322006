def longest_common_prefix(A):
    if not A:
        return ""

    # Sort the array to get the lexicographically smallest and largest strings
    A.sort()

    # Take the first and last strings after sorting
    first_str = A[0]
    last_str = A[-1]

    prefix = ""
    min_length = min(len(first_str), len(last_str))

    # Compare characters of the first and last strings
    for i in range(min_length):
        if first_str[i] == last_str[i]:
            prefix += first_str[i]
        else:
            break

    return prefix

# Example usage:
input_1 = ["abcdefgh", "aefghijk", "abcefgh"]
output_1 = longest_common_prefix(input_1)
print(output_1)

input_2 = ["abab", "ab", "abcd"]
output_2 = longest_common_prefix(input_2)
print(output_2)

