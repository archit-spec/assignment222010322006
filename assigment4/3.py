def length_of_last_word(s):
    # Initialize the length variable
    length = 0

    # Flag to track if we have encountered the first non-space character
    found_word = False

    # Iterate through the string from right to left
    for i in range(len(s) - 1, -1, -1):
        if s[i] != ' ':
            found_word = True
            length += 1
        elif found_word:
            # If we have already found the first non-space character and encounter a space, break the loop
            break

    return length

# Example usage:
input_str = "Hello World"
result = length_of_last_word(input_str)
print(result)

