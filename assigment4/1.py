def reverse_words(A):
    # Split the string into words
    words = A.split()

    # Reverse the order of the words
    reversed_words = words[::-1]

    # Join the reversed words into a string with single spaces
    reversed_string = ' '.join(reversed_words)

    return reversed_string

# Example usage:
input_str_1 = "the sky is blue"
output_str_1 = reverse_words(input_str_1)
print(output_str_1)

input_str_2 = "this is ib"
output_str_2 = reverse_words(input_str_2)
print(output_str_2)

