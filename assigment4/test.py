import unittest
import os
import glob
import importlib.util

class TestAllPythonFiles(unittest.TestCase):
    def test_run_python_files(self):
        # Specify the directory containing Python files
        directory_path = "./"

        # Get all Python files in the directory
        python_files = glob.glob(os.path.join(directory_path, "*.py"))

        # Iterate through each Python file and try to run it
        for python_file in python_files:
            file_name = os.path.basename(python_file)
            module_name = os.path.splitext(file_name)[0]

            # Try to import the module
            spec = importlib.util.spec_from_file_location(module_name, python_file)
            module = importlib.util.module_from_spec(spec)

            try:
                spec.loader.exec_module(module)
            except Exception as e:
                self.fail(f"Failed to run {file_name}: {e}")

if __name__ == '__main__':
    unittest.main()

