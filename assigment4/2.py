def add_binary(a, b):
    result = ""
    carry = 0

    # Make both strings of equal length by prepending zeros
    length = max(len(a), len(b))
    a = a.zfill(length)
    b = b.zfill(length)

    # Iterate through the strings from right to left
    for i in range(length - 1, -1, -1):
        bit_sum = int(a[i]) + int(b[i]) + carry
        result = str(bit_sum % 2) + result
        carry = bit_sum // 2

    # Check for any remaining carry
    if carry:
        result = "1" + result

    return result

# Example usage:
a = "100"
b = "11"
result = add_binary(a, b)
print(result)

