import unittest
import os
import glob
import importlib.util

class TestAllPythonFiles(unittest.TestCase):
    def test_run_python_files(self):
        # Specify the root directory
        root_directory = "./"

        # Iterate through each subdirectory
        for subdir, _, files in os.walk(root_directory):
            # Get all Python files in the current subdirectory
            python_files = [f for f in files if f.endswith(".py")]

            # Iterate through each Python file and try to run it
            for python_file in python_files:
                file_path = os.path.join(subdir, python_file)
                module_name = os.path.splitext(python_file)[0]

                # Try to import the module
                spec = importlib.util.spec_from_file_location(module_name, file_path)
                module = importlib.util.module_from_spec(spec)

                try:
                    spec.loader.exec_module(module)
                except Exception as e:
                    self.fail(f"Failed to run {file_path}: {e}")

if __name__ == '__main__':
    unittest.main()

